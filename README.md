To run the app, you need MySql running and a `.env` file in the following format:

```
DB_HOST=<your host>
DB_USER=<mysql user name>
DB_PASS=<mysql user password>
PORT=9000 // change if you're running on a different port
```


You should also set the database name in the `database.ts` file. By default it's `vf`

You can make use of the `vf-schema.sql` to create the database