import bcrypt = require('bcrypt');
import { IAPIResponse } from '../interfaces';
import { DBUtil, IQueryResult } from '../utilities/db';
import jwt = require('jsonwebtoken');

export class User extends DBUtil {
  secret = require('crypto').randomBytes(256).toString('hex');

  createUser = async (req: any, res: any) => {
    const userName = req.body.userName;
    const password = req.body.password;

    const hash = bcrypt.hashSync(password, 11);
    const query =
      'insert into Users set UserName = ?, PasswordHash = ?';
    const values = [userName, hash];

    const apiResponse: IAPIResponse = {
      isError: false,
      data: -1,
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (queryResult.isError) {
      console.log(queryResult.result)
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result.insertId;
    }
    res.send(apiResponse);
  };

  authenticateUser = async (req: any, res: any) => {
    const query = 'select UserId, PasswordHash from Users where UserName = ?';
    const values = [req.body.userName];
    const apiResponse: IAPIResponse = {
      isError: false,
      data: false,
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (!queryResult.isError) {
      if (queryResult.result.length > 0) {
        // compare pwds
        const receivedPassword = req.body.password;
        const retrievedPassword = new Buffer(queryResult.result[0].PasswordHash).toString();
        const pwdMatch = bcrypt.compareSync(receivedPassword, retrievedPassword);

        if (pwdMatch) {
          const token = jwt.sign(
            { userId: queryResult.result[0].UserId },
            this.secret,
            { expiresIn: '24h' }
          );
          apiResponse.data = token;
        }

      }
    } else {
      apiResponse.isError = true;
    }

    res.send(apiResponse);
  };

  resetPassword = async (req: any, res: any) => {
    const userName = req.body.userName;
    const password = req.body.password;
    const newPassword = req.body.newPassword;

    const query = 'select UserId, PasswordHash from Users where UserName = ?';
    const values = [userName];
    const apiResponse: IAPIResponse = {
      isError: false,
      data: false,
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (!queryResult.isError) {
      if (queryResult.result.length > 0) {
        // compare pwds
        const retrievedPassword = new Buffer(queryResult.result[0].PasswordHash).toString();
        const pwdMatch = bcrypt.compareSync(password, retrievedPassword);
        const userId = queryResult.result[0].UserId;

        if (pwdMatch) {
          // change pwd
          const hash = bcrypt.hashSync(newPassword, 11);
          const query =
            'update Users set PasswordHash = ? where UserId = ?';
          const values = [hash, userId];
          const updateResult: IQueryResult = await super.executeQuery(query, values);

          if (updateResult.isError || updateResult.result.changedRows !== 1) {
            apiResponse.isError = true;
          } else {
            const token = jwt.sign(
              { userId: queryResult.result[0].UserId },
              this.secret,
              { expiresIn: '24h' }
            );
            apiResponse.data = token;
          }
        }
      }
    } else {
      apiResponse.isError = true;
    }

    res.send(apiResponse);
  };
}
