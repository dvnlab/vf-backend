import { IAPIResponse } from '../interfaces';
import { DBUtil, IQueryResult } from '../utilities/db';

export class Annotation extends DBUtil {
  getAnnotations = async (req: any, res: any) => {
    const postId = req.params.postId;
    const query = 'select AnnotationId, Start, End, Notes, CatId from annotations where PostId = ? and IsDeleted = 0';
    const values = [postId];

    const apiResponse: IAPIResponse = {
      isError: false,
      data: [],
    };
    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (queryResult.isError) {
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result;
    }
    res.send(apiResponse);
  };

  createAnnotation = async (req: any, res: any) => {
    const postId = req.body.postId;
    const start = req.body.start;
    const end = req.body.end;

    const query = 'insert into annotations set PostId = ?, Start = ?, End = ?';
    const values = [postId, start, end];

    const apiResponse: IAPIResponse = {
      isError: false,
      data: -1,
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (queryResult.isError) {
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result.insertId;
    }
    res.send(apiResponse);
  };

  updateNote = async (req: any, res: any) => {
    const annotationId = req.body.annotationId;
    const note = req.body.note;
    const catId = req.body.catId;

    const query = 'update annotations set Notes = ?, CatId = ? where AnnotationId = ?';
    const values = [note, catId, annotationId];

    const apiResponse: IAPIResponse = {
      isError: false,
      data: -1,
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (queryResult.isError) {
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result.changedRows;
    }
    res.send(apiResponse);
  };

  deleteAnnotation = async (req: any, res: any) => {
    const annotationId = req.body.annotationId;

    const query = 'update annotations set IsDeleted = 1 where AnnotationId = ?';
    const values = [annotationId];

    const apiResponse: IAPIResponse = {
      isError: false,
      data: 0,
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (queryResult.isError) {
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result.affectedRows;
    }
    res.send(apiResponse);
  };
}
