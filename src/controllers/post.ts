import { IAPIResponse } from '../interfaces';
import { DBUtil, IQueryResult } from '../utilities/db';

export class Post extends DBUtil {
  getPost = async (req: any, res: any) => {
    const userId = req.params.userId;
    const query = 'select PostId, Post from Posts where Createdby = ?';
    const values = [userId];

    const apiResponse: IAPIResponse = {
      isError: false,
      data: [],
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (queryResult.isError) {
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result[0];
    }
    res.send(apiResponse);
  };

  createPost = async (req: any, res: any) => {
    const userId = req.body.userId;
    const post = req.body.post;

    const query = 'insert into Posts set Post = ?, Createdby = ?';
    const values = [post, userId];

    const apiResponse: IAPIResponse = {
      isError: false,
      data: -1,
    };

    const queryResult: IQueryResult = await super.executeQuery(query, values);

    if (queryResult.isError) {
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result.insertId;
    }
    res.send(apiResponse);
  };
}
