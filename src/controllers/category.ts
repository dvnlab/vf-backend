import { IAPIResponse } from '../interfaces';
import { DBUtil, IQueryResult } from '../utilities/db';

export class Category extends DBUtil {
  getCategories = async (req: any, res: any) => {
    const query = 'select CatId, Category from categories where IsActive = 1';

    const apiResponse: IAPIResponse = {
      isError: false,
      data: [],
    };
    const queryResult: IQueryResult = await super.executeQuery(query);

    if (queryResult.isError) {
      apiResponse.isError = true;
    } else {
      apiResponse.data = queryResult.result;
    }
    res.send(apiResponse);
  };
}
