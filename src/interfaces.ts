export interface IAPIResponse {
  isError: boolean;
  data: any;
}
