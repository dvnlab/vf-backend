import { Annotation } from '../controllers/annotation';
import { Post } from '../controllers/post';
import { Application } from 'express';
import { Pool } from 'mysql2/promise';
import { User } from '../controllers/user';
import * as DB from '../database';
import { Category } from '../controllers/category';


export class Routes {
  constructor(app: Application) {
    const dbPool = DB.getPool();
    this.setRoutes(app, dbPool);
  }

  private setRoutes(app: Application, dbPool: Pool) {
    const user = new User(dbPool);
    app.route('/auth/user').post(user.authenticateUser);
    app.route('/reset/user').post(user.resetPassword);
    app.route('/create/user').post(user.createUser);

    const post = new Post(dbPool);
    app.route('/create/post').post(post.createPost);
    app.route('/get/post/:userId').get(post.getPost);

    const annotation = new Annotation(dbPool);
    app.route('/get/annotation/:postId').get(annotation.getAnnotations);
    app.route('/create/annotation').post(annotation.createAnnotation);
    app.route('/update/annotation').post(annotation.updateNote);
    app.route('/delete/annotation').post(annotation.deleteAnnotation);

    const category = new Category(dbPool);
    app.route('/get/category').get(category.getCategories);
  }
}
