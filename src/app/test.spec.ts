import { Test } from './test';

describe('sample test', () => {
  it('converts input to locale upper case', () => {
    const tst = new Test();
    const input = 'abc';
    expect(tst.sample(input)).toEqual(input.toLocaleUpperCase());
  });
});
