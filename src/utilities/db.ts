import { Pool } from 'mysql2/promise';
import mysql = require('mysql2/promise');

export class DBUtil {
  constructor(private dbPool: Pool) {}

  /*
  article: https://evertpot.com/executing-a-mysql-query-in-nodejs/
  running transactions, multiple queries etc
  */

  async executeQuery(query: string, values?: any): Promise<IQueryResult> {
    let queryResult: any;
    let isError = false;

    query = mysql.format(query, values);

    try {
      const [rows] = await this.dbPool.execute(query);
      queryResult = rows;
    } catch (err) {
      queryResult = err;
      isError = true;
    }
    return {
      isError,
      result: queryResult,
    };
  }
}

export interface IQueryResult {
  isError: boolean;
  result: any;
}
