import bodyParser = require('body-parser');
import express = require('express');
import { Application } from 'express';
import { NextFunction, RequestHandler, RequestHandlerParams } from 'express-serve-static-core';
import { Routes } from './routes/routes';
// tslint:disable-next-line:no-var-requires
require('dotenv').config();

const port = process.env.PORT || '9000';
const app: Application = express();
const server = app.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log('VF server listening on port', port);
});

function setMiddleware() {
  // example
  const reqTime: RequestHandler = (req: any, res: any, next: NextFunction) => {
    req.requestTime = new Date();
    next();
  };

  const cors = (req: any, res: any, next: any) => {
    // res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
  };

  const bodyParserMiddleware = bodyParser.json();

  const middleWare = [bodyParserMiddleware, cors];
  app.use(middleWare);
}

function initRoutes() {
  const route = new Routes(app);
}

setMiddleware();
initRoutes();